// Phương thức filter() thực thi hàm được cung cấp một lần
// cho mỗi phần tử mảng và trả về tất cả các phần tử vượt qua
// bài kiểm tra bằng hàm được cung cấp. Nếu không nó sẽ trả về
// không xác định.

let products1 = [
  { name: "cucumber", type: "vegetable" },
  { name: "banana", type: "fruit" },
  { name: "celery", type: "vegetable" },
  { name: "orange", type: "fruit" },
];

// vòng lặp mảng sử dụng for thông thường

let filteredProducts1 = [];
for (let i = 0; i < products1.length; i++) {
  if (products1[i].type === "fruit") {
    filteredProducts1.push(products1[i]);
  }
}

console.log(filteredProducts1);

// Lưu ý rằng phương thức filter() không thay đổi mảng ban đầu.
// Hãy xem ví dụ với nhiều điều kiện.

let products2 = [
  { name: "cucumber", type: "vegetable", quantity: 0, price: 1 },
  { name: "banana", type: "fruit", quantity: 10, price: 15 },
  { name: "celery", type: "vegetable", quantity: 30, price: 9 },
  { name: "orange", type: "fruit", quantity: 3, price: 5 },
];

// Filter out:  (1) 'vegetable' Type
//              (2) quantity is greater than 0
//              (3) price is less than 10

let filteredProducts2 = products2.filter(
  (product) =>
    product.type === "vegetable" && product.quantity > 0 && product.price < 10
);

console.log(filteredProducts2);

// Bài 1: Lọc mảng số bằng trình trợ giúp bộ lọc,
// tạo một mảng mới chỉ chứa các số lớn hơn 50.
// Gán mảng mới này cho một biến có tên là filterNumbers.
// Đừng quên sử dụng từ khóa return trong hàm!
let numbers = [15, 25, 35, 45, 55, 65, 75, 85, 95];

let filterNumbers = numbers.filter((number) => number > 50);
console.log(filterNumbers);

// Bài 2: Lọc mảng người dùng, chỉ những người dùng cũ
// có quyền truy cập cấp quản trị viên. Gán kết quả cho
// biến filterUsers. Đừng quên sử dụng từ khóa return trong hàm!

let users = [
  { id: 1, admin: true },
  { id: 2, admin: false },
  { id: 3, admin: false },
  { id: 4, admin: false },
  { id: 5, admin: true },
];

let filterUsers = users.filter((user) => user.admin === true);

console.log(filterUsers);

// Bài 3: Tạo một hàm gọi là reject. Từ chối sẽ hoạt động theo
// cách ngược lại của bộ lọc - nếu một hàm trả về true thì mục
// đó sẽ không được đưa vào mảng mới.

var numbers2 = [10, 20, 30];
var lessThanFifteen = reject(numbers2, function (number) {
  return number > 15;
});

// returns: [10]
lessThanFifteen();

function reject(array, iteratorFunction) {
  return array.filter((item) => !iteratorFunction(item));
}
