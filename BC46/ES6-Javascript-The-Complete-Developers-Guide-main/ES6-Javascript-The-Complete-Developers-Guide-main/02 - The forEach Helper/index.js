let colors = ["red", "green", "blue", "alpha"];

// vòng lặp qua tất cả các phần tử trong mảng sử dụng for bình thường
for (let i = 0; i < colors.length; i++) {
  console.log(colors[i]);
}

// vòng lặp qua các phần tử trong mảng sử dụng forEach
colors.forEach((color) => console.log(color));

// ví dụ khác, sử dụng forEach để tính tổng một danh sách các số
let sum = [0];
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

numbers.forEach((num) => (sum += num));

// ví dụ khác
function handlePosts() {
  var posts = [
    { id: 23, title: "Daily JS News" },
    { id: 52, title: "Code Refactor City" },
    { id: 105, title: "The Brightest Ruby" },
  ];

  for (var i = 0; i < posts.length; i++) {
    savePost(posts[i]);
  }
  // trên là 1 lòng lặp for thông thường, chuyển nó thanh forEarch cho đơn giãn hơn
  posts.forEach(post => savePost(post));     
}


// Mảng bên dưới chứa một mảng các đối tượng, mỗi đối tượng là một biểu diễn của một hình ảnh.
// Sử dụng trình trợ giúp forEach(), tính diện tích của mỗi hình ảnh và lưu trữ nó trong một mảng mới gọi là các khu vực. 
// Diện tích của hình ảnh có thể được tính là image.height * image.width.
let images = [
    { height: 10, width: 30 },
    { height: 20, width: 90 },
    { height: 54, width: 32 }
  ];
  
  let areas = [];

  images.forEach(image => areas.push(image.height * image.width));