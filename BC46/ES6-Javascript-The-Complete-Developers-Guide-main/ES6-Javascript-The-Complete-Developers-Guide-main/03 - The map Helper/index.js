let numbers = [1, 2, 3];
let doubleNumbers = [];

// lặp mảng bằng cachs ử dụng for
for (let i = 0; i < numbers.length; i++) {
  doubleNumbers.push(numbers[i] * 2);
}
console.log(doubleNumbers);

// lặp mảng sử dụng map
doubleNumbers = numbers.map((number) => number * 2);
console.log(doubleNumbers);

//Lưu ý rằng phương thức map() không thay đổi mảng ban đầu.
// Hãy xem một ví dụ khác thường gặp với các khung kết xuất
// phía máy khách như React hoặc Angular.
let cars = [
  { model: "Buick", price: "cheap" },
  { model: "Camaro", price: "expensive" },
];

let prices = cars.map((car) => car.price);

console.log(prices)

// Bài 1: Sử dụng map(), tạo một mảng mới chứa thuộc tính 
// chiều cao của từng đối tượng. Gán mảng mới này cho các độ 
// cao thay đổi. Đừng quên sử dụng từ khóa return trong hàm!

var images = [
    { height: '34px', width: '39px' },
    { height: '54px', width: '19px' },
    { height: '83px', width: '75px' },
  ];
  
let heights = images.map(image => image.height);


// Bài 2: Sử dụng map(), tạo một mảng mới chứa giá trị 
// (khoảng cách/thời gian) từ mỗi chuyến đi. 
// Nói cách khác, mảng mới phải chứa giá trị (khoảng cách/thời gian). 
// Gán kết quả cho các tốc độ thay đổi.
let trips = [
    { distance: 34, time: 10 },
    { distance: 90, time: 50 },
    { distance: 59, time: 25 }
  ];
  
  let speeds = trips.map(trip => trip.distance / trip.time);
