// phạm vi hoạt động

// var functione scope

var isLogin = true;

function checkLogin() {
    console.log("message", message);
    if (isLogin) {
        let message = "Đăng nhập thằng công"
    }
}

checkLogin();

// let block scope

var is_login = true;
function check_login() {
    if (is_login) {
        let message = "Đăng nhập thành công rồi nè!!!"
        console.log("message", message);
    }
}

check_login();

// example: in ra ra số từ 0 tới 4 sau mỗi 1s
for (var i = 0; i < 5; i++) {
    setTimeout(function () {
        console.log(i);
    }, 1000);
} // => 5   5   5   5   5

for (var i = 0; i < 5; i++) {
    (function (j) {
        setTimeout(function () {
            console.log(j);
        }, 1000);
    })(i);
} // này mới ra đúng kết quả nè => 0    1   2   3   4