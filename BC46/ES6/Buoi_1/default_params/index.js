let sayHello = (user = "USER") => {
  console.log(`Hello ${user}`);
}

sayHello("CongDanh");

sayHello();

// Nếu không truyền giá trị khi gọi hàm thì sẽ lấy 'USER 