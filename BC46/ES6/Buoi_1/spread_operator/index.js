let cat1 = {
  name: "miu",
  score: 10,
}

// let cat2 = cat1;
// cat2.score = 1;
//       ||
let cat2 = { ...cat1, score: 1, age: 2 }
console.log("cat1", cat1);
console.log("cat2", cat2);

// spread operator ~ ... ~ dùng để clone, edit object hoặc array
// Nó tạo ra đối tượng mới, nếu thêm mới hoặc thay đổi thuộc tính đối tượng cũng sẽ không đổi\


// array
let colors = ["red", "black"];
let newColors = [...colors, "green"];
console.log("newColors", newColors);
colors.push("white");
console.log("newColors", newColors);

// 
let a = 2;

let b = a;
b = 1;
console.log(a);