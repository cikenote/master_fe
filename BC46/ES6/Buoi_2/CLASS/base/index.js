function Sv(_name, _email) {
  this.name = _name;
  this.email = _email;
}

class Cat {
  // Hàm khởi tạo, tự động được chạy khi new object
  constructor(_name, _age) {
    this.name = _name;
    this.age = _age;
  }

  speak() {
    console.log(`Meo meo, tao là: ${this.name}`);
  }
}

let cat1 = new Cat("mun", 2);
console.log(cat1);
cat1.speak();
