export default class Food {
  constructor(_ma, _ten, _loai, _gia, _khuyenMai, _tinhTrang, _hinhAnh, _moTa) {
    this.id = _ma;
    this.name = _ten;
    this.type = _loai;
    this.price = _gia;
    this.discount = _khuyenMai;
    this.status = _tinhTrang;
    this.image = _hinhAnh;
    this.desc = _moTa;
  }

  caculatePrice() {
    return this.price * (1 - this.discount);
  }
}
