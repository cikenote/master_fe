export function layThongTinTuForm() {
  console.log("yes");

  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let gia = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhAnh = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  return {
    ma,
    ten,
    loai,
    gia,
    khuyenMai,
    tinhTrang,
    hinhAnh,
    moTa,
  };
}

export let showThongTinLenForm = (food) => {
  let { id, name, type, price, discount, status, image, desc } = food;
  document.getElementById("imgMonAn").src = image;
  document.getElementById("spMa").innerText = id;
  document.getElementById("spLoaiMon").innerText = name;
  document.getElementById("spGia").innerText = price;
  document.getElementById("spKM").innerText = food.caculatePrice();
};
