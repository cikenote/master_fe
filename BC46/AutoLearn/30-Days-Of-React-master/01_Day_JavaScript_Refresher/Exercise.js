const countries = [
  "Albania",
  "Bolivia",
  "Canada",
  "Denmark",
  "Ethiopia",
  "Finland",
  "Germany",
  "Hungary",
  "Ireland",
  "Japan",
  "Kenya",
];

const webTechs = [
  "HTML",
  "CSS",
  "JavaScript",
  "React",
  "Redux",
  "Node",
  "MongoDB",
];

//   1. Declare an empty array;
const emptyArray = [];
//   2. Declare an array with more than 5 number of elements
const numbersArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
console.log("🚀 ~ file: Exercise.js:29 ~ numbersArray:", numbersArray);
const namesArray = ["Alice", "Bob", "David", "Fred"];
console.log("🚀 ~ file: Exercise.js:30 ~ namesArray:", namesArray);
//   3. Find the length of your array
let numbersArrayLength = numbersArray.length;
console.log(
  "🚀 ~ file: Exercise.js:34 ~ numbersArrayLength:",
  numbersArrayLength
);
console.log("Length of numbers array: ", numbersArray);
//   4. Get the first item, the middle item and the last item of the array
let firstItem = numbersArray[0];
console.log("🚀 ~ file: Exercise.js:35 ~ firstItem:", firstItem);
let middleItem = numbersArray[Math.floor(numbersArrayLength / 2)];
console.log("🚀 ~ file: Exercise.js:36 ~ middleItem:", middleItem);
let lastItem = numbersArray[Math.floor(numbersArrayLength - 1)];
console.log("🚀 ~ file: Exercise.js:40 ~ lastItem:", lastItem);
//   5. Declare an array called mixedDataTypes, put different data types in the array and find the length of the array. The array size should be greater than 5
let mixedDataTypes = [
  1,
  "Hello",
  true,
  false,
  null,
  NaN,
  { name: "Danh" },
  ["HCM", "HaVan"],
];
console.log("🚀 ~ file: Exercise.js:57 ~ mixedDataTypes:", mixedDataTypes);
let mixedDataTypesLength = mixedDataTypes.length;
console.log(
  "🚀 ~ file: Exercise.js:58 ~ mixedDataTypesLength:",
  mixedDataTypesLength
);
//   6. Declare an array variable name itCompanies and assign initial values Facebook, Google, Microsoft, Apple, IBM, Oracle and Amazon
let itCompanies = [
  "Facebook",
  "Google",
  "Microsoft",
  "Apple",
  "IBM",
  "Oracle",
  "Amazon",
];
console.log("🚀 ~ file: Exercise.js:60 ~ itCompanies:", itCompanies);
//   7. Print the array using console.log()
let mangNe = ["Danh", "Đẹp", "Trai"];
console.log("🚀 ~ file: Exercise.js:76 ~ mangNe:", mangNe);
//   8. Print the number of companies in the array
console.log("Number of companies: ", itCompanies.length);
//   9. Print the first company, middle and last company
console.log("First company:", itCompanies[0]);
console.log("Middle company:", itCompanies[Math.floor(itCompanies.length / 2)]);
console.log("Last company:", itCompanies[itCompanies.length - 1]);
console.log("--------------------------------");
//   10. Print out each company
itCompanies.forEach(function (company) {
  console.log(company);
});
console.log("--------------------------------");
//   11. Change each company name to uppercase one by one and print them out
itCompanies.forEach(function (company) {
  console.log(company.toUpperCase());
});
//   12. Print the array like as a sentence: Facebook, Google, Microsoft, Apple, IBM,Oracle and Amazon are big IT companies.
let companiesSentence = itCompanies.join(", ") + "are big IT companies.";
console.log(
  "🚀 ~ file: Exercise.js:95 ~ companiesSentence:",
  companiesSentence
);
//   13. Check if a certain company exists in the itCompanies array. If it exist return the company else return a company is not found
if (itCompanies.includes("Google")) {
  console.log("Google exists in the itCompanies array");
} else {
  console.log("Google is not in the itCompanies array");
}
//   14. Filter out companies which have more than one 'o' without the filter method
let companiesWithoutO = [];
for (let company of itCompanies) {
  if ((company.match(/o/) || []).length < 2) {
    companiesWithoutO.push(company);
  }
}
console.log("Companies without more than one 'o':", companiesWithoutO);
//   15. Sort the array using sort() method
itCompanies.sort();
console.log("Sorted array:", itCompanies);
//   16. Reverse the array using reverse() method
itCompanies.reverse();
console.log("Reversed array:", itCompanies);
//   17. Slice out the first 3 companies from the array
let firstThreeCompanies = itCompanies.slice(0, 3);
console.log("First three companies:", firstThreeCompanies);
//   18. Slice out the last 3 companies from the array
let lastThreeCompanies = itCompanies.slice(-3);
console.log("Last three companies:", lastThreeCompanies);
//   19. Slice out the middle IT company or companies from the array
let middleIndex = Math.floor(itCompanies.length / 2);
let middleCompanies = [];
if (itCompanies.length % 2 === 0) {
  middleCompanies.push(itCompanies[middleIndex - 1]);
}
middleCompanies.push(itCompanies[middleIndex]);
console.log("Middle company(s):", middleCompanies);
//   20. Remove the first IT company from the array
itCompanies.shift();
console.log("Array after removing first company:", itCompanies);
//   21. Remove the middle IT company or companies from the array
if (itCompanies.length % 2 === 0) {
  itCompanies.splice(middleIndex - 1, 2);
} else {
  itCompanies.splice(middleIndex, 1);
}
console.log("Array after removing middle company:", itCompanies);
//   22. Remove the last IT company from the array
itCompanies.pop();
console.log("Array after removing last company:", itCompanies);
//   23. Remove all IT companies
itCompanies = [];
console.log("Array after removing all companies:", itCompanies);
