class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class SinglyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  addToHead(x) {
    const newNode = new Node(x);
    if (!this.head) {
      this.head = newNode;
      this.tail = newNode;
    } else {
      newNode.next = this.head;
      this.head = newNode;
    }
    this.length++;
  }

  addToTail(x) {
    const newNode = new Node(x);
    if (!this.tail) {
      this.head = newNode;
      this.tail = newNode;
    } else {
      this.tail.next = newNode;
      this.tail = newNode;
    }
    this.length++;
  }

  addAfter(p, x) {
    let currentNode = this.head;
    while (currentNode) {
      if (currentNode.value === p) {
        const newNode = new Node(x);
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        if (currentNode === this.tail) {
          this.tail = newNode;
        }
        this.length++;
        break;
      }
      currentNode = currentNode.next;
    }
  }

  traverse() {
    let currentNode = this.head;
    while (currentNode) {
      console.log(currentNode.value);
      currentNode = currentNode.next;
    }
  }

  deleteFromHead() {
    if (!this.head) return null;
    const deletedNode = this.head;
    if (this.head === this.tail) {
      this.head = null;
      this.tail = null;
    } else {
      this.head = this.head.next;
    }
    this.length--;
    return deletedNode.value;
  }

  deleteFromTail() {
    if (!this.tail) return null;
    const deletedNode = this.tail;
    if (this.head === this.tail) {
      this.head = null;
      this.tail = null;
    } else {
      let currentNode = this.head;
      while (currentNode.next !== this.tail) {
        currentNode = currentNode.next;
      }
      currentNode.next = null;
      this.tail = currentNode;
    }
    this.length--;
    return deletedNode.value;
  }

  deleteAfter(p) {
    let currentNode = this.head;
    while (currentNode) {
      if (currentNode.value === p && currentNode.next) {
        const deletedNode = currentNode.next;
        currentNode.next = currentNode.next.next;
        if (deletedNode === this.tail) {
          this.tail = currentNode;
        }
        this.length--;
        return deletedNode.value;
      }
      currentNode = currentNode.next;
    }
    return null;
  }

  del(x) {
    if (!this.head) return false;
    if (this.head.value === x) {
      this.deleteFromHead();
      return true;
    }
    let currentNode = this.head;
    while (currentNode.next) {
      if (currentNode.next.value === x) {
        currentNode.next = currentNode.next.next;
        if (currentNode.next === this.tail) {
          this.tail = currentNode;
        }
        this.length--;
        return true;
      }
      currentNode = currentNode.next;
    }
    return false;
  }

  search(x) {
    let currentNode = this.head;
    while (currentNode) {
      if (currentNode.value === x) {
        return currentNode;
      }
      currentNode = currentNode.next;
    }
    return null;
  }

  count() {
    return this.length;
  }

  delByIndex(i) {
    if (i < 0 || i >= this.length) return false;
    if (i === 0) {
      this.deleteFromHead();
      return true;
    }
    let currentIndex = 0;
    let currentNode = this.head;
    let previousNode = null;
    while (currentIndex < i) {
      previousNode = currentNode;
      currentNode = currentNode.next;
      currentIndex++;
    }
    previousNode.next = currentNode.next;
    if (previousNode.next === this.tail) {
      this.tail = previousNode;
    }
    this.length--;
    return true;
  }

  sort() {
    let swapped;
    do {
      swapped = false;
      let currentNode = this.head;
      let previousNode = null;
      while (currentNode.next) {
        if (currentNode.value > currentNode.next.value) {
          const temp = currentNode.value;
          currentNode.value = currentNode.next.value;
          currentNode.next.value = temp;
          swapped = true;
        }
        previousNode = currentNode;
        currentNode = currentNode.next;
      }
    } while (swapped);
  }

  toArray() {
    const array = [];
    let currentNode = this.head;
    while (currentNode) {
      array.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return array;
  }

  merge(otherList) {
    if (!otherList.head) return;
    if (!this.head) {
      this.head = otherList.head;
      this.tail = otherList.tail;
    } else {
      this.tail.next = otherList.head;
      this.tail = otherList.tail;
    }
    this.length += otherList.length;
  }

  addBefore(p, x) {
    if (!this.head) return;
    if (this.head.value === p) {
      this.addToHead(x);
      return;
    }
    let currentNode = this.head;
    while (currentNode.next) {
      if (currentNode.next.value === p) {
        const newNode = new Node(x);
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        this.length++;
        return;
      }
      currentNode = currentNode.next;
    }
  }

  attach(otherList) {
    if (!otherList.head) return;
    if (!this.head) {
      this.head = otherList.head;
      this.tail = otherList.tail;
    } else {
      this.tail.next = otherList.head;
      this.tail = otherList.tail;
    }
    this.length += otherList.length;
  }

  max() {
    if (!this.head) return null;
    let max = this.head.value;
    let currentNode = this.head;
    while (currentNode) {
      if (currentNode.value > max) {
        max = currentNode.value;
      }
      currentNode = currentNode.next;
    }
    return max;
  }

  min() {
    if (!this.head) return null;
    let min = this.head.value;
    let currentNode = this.head;
    while (currentNode) {
      if (currentNode.value < min) {
        min = currentNode.value;
      }
      currentNode = currentNode.next;
    }
    return min;
  }

  sum() {
    let sum = 0;
    let currentNode = this.head;
    while (currentNode) {
      sum += currentNode.value;
      currentNode = currentNode.next;
    }
    return sum;
  }

  avg() {
    if (!this.head) return null;
    return this.sum() / this.length;
  }

  sorted() {
    let currentNode = this.head;
    while (currentNode && currentNode.next) {
      if (currentNode.value > currentNode.next.value) {
        return false;
      }
      currentNode = currentNode.next;
    }
    return true;
  }

  insert(x) {
    if (!this.head || x < this.head.value) {
      this.addToHead(x);
      return;
    }
    let currentNode = this.head;
    while (currentNode.next && currentNode.next.value < x) {
      currentNode = currentNode.next;
    }
    const newNode = new Node(x);
    newNode.next = currentNode.next;
    currentNode.next = newNode;
    if (currentNode === this.tail) {
      this.tail = newNode;
    }
    this.length++;
  }

  reverse() {
    let currentNode = this.head;
    let previousNode = null;
    let nextNode = null;
    while (currentNode) {
      nextNode = currentNode.next;
      currentNode.next = previousNode;
      previousNode = currentNode;
      currentNode = nextNode;
    }
    this.tail = this.head;
    this.head = previousNode;
  }

  isEqualTo(otherList) {
    if (this.length !== otherList.length) return false;
    let currentNode1 = this.head;
    let currentNode2 = otherList.head;
    while (currentNode1 && currentNode2) {
      if (currentNode1.value !== currentNode2.value) {
        return false;
      }
      currentNode1 = currentNode1.next;
      currentNode2 = currentNode2.next;
    }
    return true;
  }
}

// Example usage:
const linkedList = new SinglyLinkedList();
linkedList.addToHead(3);
linkedList.addToHead(2);
linkedList.addToHead(1);
linkedList.addToTail(4);
linkedList.addToTail(5);
linkedList.traverse(); // Output: 1 2 3 4 5
console.log("----------------Delete From Head----------------");
console.log(linkedList.deleteFromHead()); // Output: 1
console.log("----------------Delete From Tail----------------");
console.log(linkedList.deleteFromTail()); // Output: 5
console.log("--------------------------------");

linkedList.addAfter(2, 6);
linkedList.traverse(); // Output: 2 3 6 4
console.log("--------------------------------");

linkedList.del(3);
linkedList.traverse(); // Output: 2 6 4

console.log(linkedList.search(6)); // Output: Node { value: 6, next: Node { value: 4, next: null } }

console.log(linkedList.count()); // Output: 3

linkedList.delByIndex(1);
linkedList.traverse(); // Output: 2 4

linkedList.sort();
linkedList.traverse(); // Output: 2 4

console.log(linkedList.toArray()); // Output: [2, 4]

const otherList = new SinglyLinkedList();
otherList.addToTail(7);
otherList.addToTail(8);

linkedList.merge(otherList);
linkedList.traverse(); // Output: 2 4 7 8

linkedList.addBefore(4, 9);
linkedList.traverse(); // Output: 2 9 4 7 8

const anotherList = new SinglyLinkedList();
anotherList.addToTail(10);
anotherList.addToTail(11);

linkedList.attach(anotherList);
linkedList.traverse(); // Output: 2 9 4 7 8 10 11

console.log(linkedList.max()); // Output: 11
console.log(linkedList.min()); // Output: 2
console.log(linkedList.sum()); // Output: 51
console.log(linkedList.avg()); // Output: 7.285714285714286

console.log(linkedList.sorted()); // Output: false

linkedList.insert(5);
linkedList.traverse(); // Output: 2 4 5 7 8 9 10 11

linkedList.reverse();
linkedList.traverse(); // Output: 11 10 9 8 7 5 4 2

const newList = new SinglyLinkedList();
newList.addToHead(2);
newList.addToTail(4);
console.log(linkedList.isEqualTo(newList)); // Output: false
