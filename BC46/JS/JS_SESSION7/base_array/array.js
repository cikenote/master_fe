var foodList = ["hu tieu", "com", "banh mi"];
// array ~ mảng ~ [] ~ dấu phẩy
// phương thức đi kèm array
// lấy độ dài, số lượng phần tử
var tongMon = foodList.length;
console.log(tongMon);

//duyệt và in ra danh sách món


// thêm phần tử array
foodList.push("pho");
console.log(foodList);

// update 1 phần tử trong array
foodList[3] = "pho dac biet";
console.log(foodList);


//duyệt mãng forEach
foodList.forEach(function(monAn){
    console.log("item", monAn);
});


for(var i = 0; i < foodList.length; i++){
    console.log("mon", foodList[i]);
}

//callback function