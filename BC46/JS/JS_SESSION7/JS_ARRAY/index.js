/** Vấn đề đặt ra: Cần lưu trữ 5 thông thong là tên học viên (Nam, Hằng, Minh, Long, Khải) */
// Khai báo mảng
var arrName = ['Nam', 'Hằng','Minh', 'Long', 'Khải'];
// Lấy ra 1 giá trị trong mảng
console.log(arrName[2]);
arrName[1]='Sang';
// arrName.length = 5;
// Lấy ra chiều dài (sl phần tử array)
// option + esc | ctrl + space
console.log('chiều dài',arrName.length);
var content = '';
// Duyệt mảng: In ra màn hình của browser
for(var index = 0; index<arrName.length; index++){
    console.log(arrName[index]);
    content += `<p class="badge badge-success">${arrName[index]}</p>`
}
document.getElementById('content-array').innerHTML = content;
/**-----------------Hàm thêm value vào array----------------- */
// Hàm thêm 1 value vào array: push()
arrName.push('Uyên');
// Hàm thêm 1 value vào đầu array: unshift() => làm thay đổi index nên ít sd
arrName.unshift('Bảo');


/**-----------------Hàm xóa value vào array----------------- */
// Xóa 1 or nhiều value trong array: splice(index,quantity) => thay đổi index và length
arrName.splice(2,1);
console.log(arrName);
// shift(): Lấy ra 1 phần tử đầu array và xóa, pop(): lấy ra phần tử cuối array và xóa
var hoTen1 = arrName.shift();
console.log('hoTen', hoTen1);


/**-----------------DOM qua tagname----------------- */

var arrTagSection = document.getElementsByTagName('section');

arrTagSection[1].innerHTML = 'Hello Cybersoft';
arrTagSection[1].style.color = 'blue';

console.log('arrTagSection',arrTagSection);

for(var index = 0; index < arrTagSection.length;index++){
    arrTagSection[index].className = 'badge badge-danger m-2';
}

/**-----------------DOM qua classname----------------- */
var arrTagClass = document.getElementsByClassName('txt');
console.log('arrTagClass',arrTagClass);

for(var index = 0; index < arrTagClass.length; index++){
    // Mỗi lần duyệt lấy ra 1 tag
    var tag = arrTagClass[index];
    tag.className = 'txt alert alert-danger';
    tag.innerHTML = 'Hello Cybersoft';
}

/**-----------------DOM qua name----------------- */
var arrTagName = document.getElementsByName('text-demo');
console.log('arrTagName',arrTagName);
arrTagName[0].style.color='pink';
for (var index=0;index<arrTagName.length;index++){
    var tag = arrTagName[index];
    tag.className = 'txt alert alert-danger';
    tag.innerHTML = 'Hello Cybersoft';
}


/**
 * .querySelector(selector): khi dom đến dựa vào querySelector thì kết quả trả về là 1 tag đầu tiên khớp vs selector đó (Dù có nhiều tag có selector giống nhau). Nếu k có tag nào khớp với selector đó thì trả về undefined
 */

document.querySelector('#btnSubmit').onclick = function() {
    // alert(123);
    var pText1 = document.querySelector('.demo-selector .p-text:nth-of-type(2)').innerHTML;
    alert(pText1);
}

/**
 * .querySelectorAll(selector): khi DOM đến dựa vào querySelectorAll thì kết quả trả về là 1 mảng. Lưu ý: Nếu như chỉ có 1 thẻ khớp với selector thì nó vẫn trả về 1 mảng và mảng đó có 1 phần tử. Nếu không khớp phần tử nào thì kết quả là []
 */

// var btn = document.querySelectorAll('#btnSubmit');
// btn[0].style.color = 'red';
// console.log(btn);

document.querySelector('#btnDangNhap').onclick = function(event) {
    event.preventDefault(); //Hàm chặn sự kiện reload mặc định của browser khi submit
    var arrTagInput = document.querySelectorAll('#form input');
    console.log('taiKhoan',arrTagInput[0].value);
    console.log('matKhau',arrTagInput[1].value);

    console.log('abc',arrTagInput);
}
