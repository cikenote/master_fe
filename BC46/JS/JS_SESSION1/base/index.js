// chỉ chạy 1 lần duy nhất theo thứ tự từ trên xuống dưới
var a = 0;
var b = 1;
var c;
var userName = "Alice";
var isLogin = true; //boolean: true ~ false
var isMarried = null; //null
var is_married = undefined; //undefined
c = a + b;
console.log(c);
console.log("Hello user");

//++x => priority
// x++ => not priority
var no1 = 12;
var no2 = ++no1 - 2;
console.log(no2);
var no3 = 49;
var no4 = 0;
no4 = Math.sqrt(no4);
console.log(no4);

// Ex.1
var dateOfWork = 24;
var index = 100000;
var salary = null;
salary = dateOfWork * index;
console.log(salary);

// Ex.2
var no1 = 14;
var no2 = 254
var no3 = 78;
var no4 = 96;
var no5 = 347;
var average = null;
average = (no1+no2+no3+no4+no5) / 5;
console.log(average);

// Ex.3
var dollar = 23500;

var a, b, c;
a = 10;
a+= a;
console.log(a);

b = ++a + 5;
c = a++ + 5;
console.log(a);
a = 0;
console.log(b);
console.log(c);