function inNoiDung(id,noiDung){
    // input: tham số
    document.getElementById(id).innerHTML = noiDung;
    document.getElementById(id).style.fontSize = '30px';
    // output
    // khai báo hàm: (chưa được gọi)
}
// gọi hàm
inNoiDung('pThongTin', 'Hello BootCamp46');
/**
 * Hàm có giá trị trả về function return
 */
function tinhLuong(soGioLam, tienLuongTrenGio){
    // process
    var tienLuong = soGioLam * tienLuongTrenGio;
    // output
    return tienLuong;
}

/**Ví dụ 2 */
document.getElementById('btnTinhDiem').onclick = function() {
    // input
    var diemToan = Number(document.getElementById('diemToan').value);
    var diemLy = Number(document.getElementById('diemLy').value);
    var diemHoa = Number(document.getElementById('diemHoa').value);
    // output: number
    var diemTBKA = tinhDiemTB(diemToan, diemLy, diemHoa);

    document.getElementById('diemTBKA').value = diemTBKA;
    // Tính điểm trung bình khối C
    var diemVan = Number(document.getElementById('diemVan').value);
    var diemSu = Number(document.getElementById('diemSu').value);
    var diemDia = Number(document.getElementById('diemDia').value);
    // var diemTrungBinhKhoiC
    var diemTBKC = tinhDiemTB(diemVan, diemSu, diemDia);
    document.getElementById('diemTBKC').value = diemTBKC;
}
function tinhDiemTB(diem1, diem2, diem3){
    // input
    var dtb = 0;
    dtb = diem1 + diem2 + diem3;
    dtb /=3;
    // output: diemTrungBinh
    return dtb;
}

/**
 * Bài tập 1: Xây dựng chức năng zoomin zoomout cho 2 button bên dưới
 */
document.getElementById('btnZoomInHome').onclick = function() {
    zoomFontSize(5,'home');
}
document.getElementById('btnZoomOutHome').onclick = function() {
    zoomFontSize(-5,'home');
}
document.getElementById('btnZoomInText').onclick = function() {
    zoomFontSize(1,'txtContent');
}
document.getElementById('btnZoomOutText').onclick = function() {
    zoomFontSize(-1,'txtContent');
}
function zoomFontSize(size, id){
    var tag = domID(id);
    var fontSize = Number(tag.style.fontSize.replace('px',''));
    fontSize += size;
    tag.style.fontSize = fontSize + 'px';
}
// Hàm trả về 1 tag sau khi dom
function domID(id){
    return document.getElementById(id);
}