// definition
function sayHello() {
    console.log("hello");
}
// execute ~ use ~ call function
sayHello();
// tham số
function sayHelloByName(username){
    console.log('Hello',username);
}
sayHelloByName('Alice');
sayHelloByName('Bob');

function tinhDTB(toan,van){
    var dtb = (toan + van)/2;
    console.log("dtb",dtb);
}
var dtb1 = tinhDTB(8,9);
console.log("dtb1", dtb1);
var dtb2 = tinhDTB(5,9);
console.log("dtb2", dtb2);
// ko tham số, có tham số, giá trị trả về (return)
// return => trả về giá trị và dừng function tại đó
function introduce(name, age) {
    console.log(`Tôi là: ${name} - ${age} : tuổi`);
}
introduce("Alice",2);