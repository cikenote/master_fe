const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
// nhập vào loại xe và trả về số tiền km đầu tiên theo từng loại xe

function tinhGiaTienKmDauTien(loaiXe) {
    // var ketQua = 0;
    if(loaiXe == UBER_CAR){
        return 8000;
    } else if(loaiXe == UBER_SUV){
        return 9000;
    } else{
        return 10000;
    }
}

function tinhGiaTienKm1Den19(loaiXe) {
    if(loaiXe == UBER_CAR){
        return 7500;
    } else if(loaiXe == UBER_SUV){
        return 8500;
    } else{
        return 9500;
    }
}
function tinhGiaTienKm19TroDi(loaiXe){
    // switch case 
    switch(loaiXe){
        case UBER_CAR: {
            return 7000;
        }
        case UBER_SUV: {
            return 8000;
        }
        case UBER_BLACK: {
            return 9000;
        }
    }
}

function tinhTienUber() {
    // input
    var loaiXe = document.querySelector('input[name="selector"]:checked').value;
    // console.log("loaiXe", loaiXe);
    var soKm = document.getElementById('txt-km').value*1;
    console.log({loaiXe,soKm});
    // tính giá tiền theo km của từng loại xe
    var giaTienKmDauTien = tinhGiaTienKmDauTien(loaiXe);
    var giaTienKm1Den19 = tinhGiaTienKm1Den19(loaiXe);
    var giaTienKm19TroDi = tinhGiaTienKm19TroDi(loaiXe);
    // áp dụng công thức để tính tiền phải trả
    console.log({
        giaTienKmDauTien,
        giaTienKm1Den19,
        giaTienKm19TroDi,
        soKm
    });
    var tongTien = 0;
    //else if 
    if(soKm <= 1) {
        tongTien = giaTienKmDauTien * soKm;
    } else if (soKm <= 19) {
        tongTien = giaTienKmDauTien + (soKm - 1)*giaTienKm1Den19;
    } else {
        tongTien = giaTienKmDauTien + 18*giaTienKm1Den19 + (soKm - 19)*giaTienKm19TroDi;
    }
    document.querySelector("#divThanhTien").style.display = "block";
    document.querySelector("#xuatTien").innerHTML = tongTien.toLocaleString();
}