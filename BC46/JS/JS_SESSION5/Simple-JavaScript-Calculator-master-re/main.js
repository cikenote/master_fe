function isNumber(char) {
  // Hàm kiểm trả xem một ký tự có phải là một số hay không
  // charr: ký tự cần kiểm tra
  // Sử dụng biểu thức chính quy để kiểm tra khớp ký tự với một số từ 0 đến 9
  // /^d$/:
  //   - ^: Bắt đầu của chuỗi
  //   - \d: Biểu thức đại diện cho bất kỳ ký tự số nào
  //   - $: Kết thúc của chuỗi
  // Sử dụng phương thức test() của biểu thức chính quy để kiểm tra khớp ký tự với biểu thức
  return /^\d$/.test(char);
  // Nếu ký tự khớp với biểu thức chính quy, trả về true
  // Ngược lại, trả về false
}


document.getElementById("answer").readOnly = true;
// Đặt thuộc tính "readOnly" của phần tử có id là "answer" thành giá trị true.
// Điều này sẽ làm cho phần tử không thể chỉnh sửa (chỉ đọc).
let screen = document.getElementById("answer");
let buttons = document.querySelectorAll("button");
let screenValue = "";
let maxItems = 6;