// Bài 1: Quản lý tuyển sinh
document.getElementById('btnPart1').onclick = function(){
    // input: number, string
    var diemChuan = document.getElementById('diemChuan').value*1;
    var khuVuc = document.getElementById('khuVuc').value;
    var doiTuong = document.getElementById('doiTuong').value;
    var diemMon1 = document.getElementById('diemMon1').value*1;
    var diemMon2 = document.getElementById('diemMon2').value*1;
    var diemMon3 = document.getElementById('diemMon3').value*1;
    var diemKhuVuc = checkKhuVuc(khuVuc);
    var diemDoiTuong = checkDoiTuong(doiTuong);
    // ouput: string
    var isKetQua = '';
    var isThongBao = '';
    var diemTong = 0;
    // process
    const isDau = 'đậu';
    const isRot = 'rớt';
    if(checkInitialFail(diemMon1,diemMon2,diemMon3)){
        isThongBao = 'Do có điểm nhỏ hơn hoặc bằng 0';
        isKetQua = isRot;
    }else{
        diemTong = tinhTongDiem(diemDoiTuong,diemKhuVuc,diemMon1,diemMon2,diemMon3);
        isThongBao = `Tổng điểm: ${diemTong}`;
        if(diemTong>=diemChuan){
            isKetQua = isDau;
        }
        else{
            isKetQua = isRot;
        }
    }
    document.getElementById('ketQua1').innerText = `Bạn đã ${isKetQua}. ${isThongBao}`;
}
// function ktra nếu có 1 trong 3 môn bị điểm 0
function checkInitialFail(diemMon1, diemMon2, diemMon3) {
    var isFail = false;
    if(diemMon1==0||diemMon2==0||diemMon3==0){
        isFail = true;
    }
    return isFail;
}
// function ktra diem khu vực
function checkKhuVuc(khuVuc){
    var diemKhuVuc = 0;
    switch(khuVuc){
        case 'A':
            diemKhuVuc = 2
        break;
        case 'B':
            diemKhuVuc = 1
        break;
        case 'C':
            diemKhuVuc = .5
        break;
        default: diemKhuVuc = 0;
    }
    return diemKhuVuc;
}
// function ktra diem doi tuong
function checkDoiTuong(doiTuong){
    var diemDoiTuong = 0;
    switch(khuVuc){
        case '1':
            diemDoiTuong = 2.5
        break;
        case '2':
            diemDoiTuong = 1.5
        break;
        case '3':
            diemDoiTuong = 2
        break;
        default: diemDoiTuong = 0;
    }
    return diemDoiTuong;
}
// function tính tổng điểm
function tinhTongDiem(diemDoiTuong, diemKhuVuc, diemMon1, diemMon2, diemMon3){
    var tongDiem = diemDoiTuong + diemKhuVuc + diemMon1 + diemMon2 + diemMon3;
    return tongDiem;
}
// Bài 2: Tính tiền điện
document.getElementById('btnPart2').onclick = function(){
    // input: string hoTen, number soKw
    var hoTen = document.getElementById('hoTen').value;
    var soKw = document.getElementById('soKw').value*1;
    // output: string thông báo tiền diện
    var giaDien = tinhTienDien(soKw);
    document.getElementById('ketQua2').innerHTML = `Họ Tên: ${hoTen}; Tiền điện: ${giaDien.toLocaleString()}`;
}
function tinhTienDien(soKw){
    // input: tham số soKw
    // output: gia dien 
    var giaDien = 0;
    // process
    const giaMuc1 = 500;
    const giaMuc2 = 650;
    const giaMuc3 = 850;
    const giaMuc4 = 1100;
    const giaMucCuoi = 1300;

    const giaDien1 = giaMuc1*50;
    const giaDien2 = giaDien1 + giaMuc2*50;
    const giaDien3 = giaDien2 + giaMuc3*100;
    const giaDien4 = giaDien3 + giaMuc4*150;

    if(soKw<=50){
        giaDien = soKw * giaMuc1;
    } else if(soKw<=100){
        giaDien = giaDien1 + (soKw-50)*giaMuc2;
    } else if(soKw<=200){
        giaDien = giaDien2 + (soKw-100)*giaMuc3;
    } else if(soKw<=350){
        giaDien = giaDien3 + (soKw-200)*giaMuc4;
    } else{
        giaDien = giaDien4 + (soKw-350)*giaMucCuoi;
    }
    return giaDien;
}
// Bài 3: Tính thuế thu nhập cá nhân
document.getElementById('btnPart3').onclick = function(){
    // input: string hoTen, number thuNhapNam, number soNguoiPhuThuoc
    var hoTen = document.getElementById('hoTen_3').value;
    var thuNhapNam = document.getElementById('thuNhapNam').value*1;
    var soNguoiPhuThuoc = document.getElementById('soNguoiPhuThuoc').value*1;
    // output: string hoTen, number tienThue
    var tienThue = 0;
    // process
    var thuNhapChiuThue = tinhThuNhapChiuThue(thuNhapNam,soNguoiPhuThuoc);
    var tiSuatThue = tinhTiSuatThue(thuNhapChiuThue);
    tienThue = tinhThue(thuNhapChiuThue,tiSuatThue);
    if(tienThue<0){
        tienThue = 0;
        alert('Số tiền thu nhập không hợp lệ');
    }
    document.getElementById('ketQua3').innerHTML = `Họ tên: ${hoTen}; Tiền thuế thu nhập cá nhân: VND ${tienThue.toLocaleString()}`;
}
// function tính thu nhập chịu thuế
function tinhThuNhapChiuThue(thuNhapNam,soNguoiPhuThuoc){
    var thuNhapChiuThue = thuNhapNam - 4e+6 -soNguoiPhuThuoc*1.6e+6;
    return thuNhapChiuThue;
}
// function tỉ xuất thuế
function tinhTiSuatThue(thuNhapChiuThue){
    var tiSuatThue = 0;
    if(thuNhapChiuThue<=60e+6){
        tiSuatThue = 0.05;
    } else if(thuNhapChiuThue<=120e+6||thuNhapChiuThue>60e+6){
        tiSuatThue = .1;
    } else if(thuNhapChiuThue<=210e+6||thuNhapChiuThue>120e+6){
        tiSuatThue = .15;
    } else if(thuNhapChiuThue<=624e+6||thuNhapChiuThue>384e+6){
        tiSuatThue = .25;
    } else if(thuNhapChiuThue<=960e+6||thuNhapChiuThue>624e+6){
        tiSuatThue = .30;
    } else{
        tiSuatThue = .35;
    }
    return tiSuatThue;
}
// function tính thuế
function tinhThue(thuNhapChiuThue,tiSuatThue){
    var tienThue = thuNhapChiuThue*tiSuatThue;
    return tienThue;
}

// Bài 4: Tính tiền cáp
// function onchange input id loaiKhachHang 
document.getElementById('loaiKhachHang').onchange = function() {
    var cusType = document.getElementById('loaiKhachHang').value;
    var typeOfInput = typeOfCustomer(cusType);
    document.getElementById('soKetNoi').type = typeOfInput;
}
function typeOfCustomer(cusType) {
    // input: string id loại khách hàng
    var typeOfInput = 'hidden';
    if (cusType == '2') {
        typeOfInput = 'number';
    } else{
        typeOfInput = 'hidden';
    }
    return typeOfInput;
}
// function onclick 
// output: tongTienCap
var tongTienCap = 0;
document.getElementById('btnPart4').onclick = function() {
    // input
    var loaiKhachHang = document.getElementById('loaiKhachHang').value;
    // check đã nhập loại khách hàng chưa
    if(loaiKhachHang=='Chọn loại khách hàng'){
        alert('Hãy chọn loại khách hàng');
        return;
    }
    var idKhachHang = document.getElementById('idKhachHang').value;
    var soKenhPremium = document.getElementById('soKenhPremium').value*1;
    var soKetNoi = document.getElementById('soKetNoi').value;
    // output: tổng tiền cáp
    if(loaiKhachHang=='1'){
        tongTienCap = tinhMucNhaDan(soKenhPremium);
    } else{
        tongTienCap = tinhMucDoanhNghiep(soKenhPremium,soKetNoi);
    }
    document.getElementById('ketQua4').innerHTML = `Mã khách hàng: ${idKhachHang}; Tiền cáp: $${tongTienCap}`;
}
// function tinhMucNhaDan
function tinhMucNhaDan(soKenhPremium){
    const giaKenhPremium = 7.5;
    const phiXuLyInvoice = 4.5;
    const phiPrimaryServices = 20.5;

    tongTienCap = phiXuLyInvoice + phiPrimaryServices + soKenhPremium*giaKenhPremium;

    return tongTienCap;
}
// function tinhMucDoanhNghiep
function tinhMucDoanhNghiep(soKenhPremium, soKetNoi){
    const giaKenhPremium = 15;
    const phiXuLyInvoiceLv1 = 75;
    const phiXuLyInvoiceLv2 = 5;
    const phiPrimaryServices = 50;
    const tongTienCapLv1 = phiXuLyInvoiceLv1 + phiPrimaryServices + soKenhPremium * giaKenhPremium;
    if(soKetNoi<=10){
        tongTienCap = tongTienCapLv1;
    } else {
        tongTienCap = tongTienCapLv1 + (soKetNoi-10)*phiXuLyInvoiceLv2;
    }
    return tongTienCap;
}