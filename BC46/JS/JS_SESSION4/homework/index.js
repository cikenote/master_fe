// Bài 1
document.getElementById('btnSec1').onclick = function Sec1() {
    // input: 3 số nguyên
    var no1st = document.getElementById('no1st').value*1;
    var no2nd = document.getElementById('no2nd').value*1;
    var no3rd = document.getElementById('no3rd').value*1;
    // output: string thứ tự số nguyên tăng dần
    var resultSec1 = '';
    // progress: bubble sort method
    var temp = 0;
    if (no1st>no2nd){
        temp = no1st;
        no1st = no2nd;
        no2nd = temp;
    }
    if (no1st>no3rd){
        temp = no1st;
        no1st = no3rd;
        no3rd = temp;
    }
    if (no2nd>no3rd){
        temp = no2nd;
        no2nd = no3rd;
        no3rd = temp;
    }
    document.getElementById('resultSec1').innerText = `Tăng dần: ${no1st}, ${no2nd}, ${no3rd}`;
    }
    // Bài 2
document.getElementById('btnSec2').onclick = function Sec2() {
    // input: value option
    var familyRole = document.getElementById('familyRole').value;
    // output: role name
    var resultSec2 = '';
    // progress: switch case
    switch (familyRole) {
        case 'B':
            resultSec2 = "Bố";
            break;
        case 'M':
            resultSec2 = "Mẹ";
            break;
        case 'A':
            resultSec2 = "Anh trai";
            break;
        case 'E':
            resultSec2 = "Em gái";
            break;
        default:
            resultSec2 = "Not defined";
    }
    document.getElementById('resultSec2').innerHTML = `Xin chào thành viên ${resultSec2}`;
}
// Bài 3
document.getElementById('btnSec3').onclick = function Sec3() {
    // input: 3 interger numbers
    var noInt1st = document.getElementById('noInt1st').value*1;
    var noInt2nd = document.getElementById('noInt2nd').value*1;
    var noInt3rd = document.getElementById('noInt3rd').value*1;
    // output: string tổng số lẻ & chẵn
    var resultSec3 = '';
    var oddSum = 0;
    var evenSum = 0;
    // progress
    if(noInt1st%2 == 0){
        evenSum++;
    }else{
        oddSum++;
    }
    if(noInt2nd%2 == 0){
        evenSum++;
    }else{
        oddSum++;
    }
    if(noInt3rd%2 == 0){
        evenSum++;
    }else{
        oddSum++;
    }

    document.getElementById('resultSec3').innerHTML = `Tổng số lẻ: ${oddSum} - Tổng số chẵn: ${evenSum}`;
}
// Bài 4
document.getElementById('btnSec4').onclick = function Sec4() {
    // input: độ dài 3 cạnh tam giác
    var edge1 = document.getElementById('edge1').value*1;
    var edge2 = document.getElementById('edge2').value*1;
    var edge3 = document.getElementById('edge3').value*1;
    // output: tên loại tam giác
    var resultSec4 = '';
    // progress
    if(edge1==edge2==edge3){
        resultSec4 = 'tam giác đều';
    }
    else if(edge1 == edge2 || edge2 == edge3 || edge1 == edge3){
        resultSec4 = 'tam giác cân';
    }
    else{
        // bubble sort: sắp xếp kích thước cạnh tam giác tăng dần => edge3 là cạnh huyền
        var temp = '';
        if (edge1>edge2){
            temp = edge1;
            edge1 = edge2;
            edge2 = temp;
        }
        if (edge1>edge3){
            temp = edge1;
            edge1 = edge3;
            edge3 = temp;
        }
        if (edge2>edge3){
            temp = edge2;
            edge2 = edge3;
            edge3 = temp;
        }
        // kiểm tra căn bậc bình phương 2 cạnh góc vuông
        var temp2 = Math.sqrt(edge1*edge1 + edge2*edge2)
        if(edge3<edge1+edge2 && edge3 == temp2){
            resultSec4 = 'tam giác vuông';
        } else{
            resultSec4 = 'tam giác thường';
        }
    }
    document.getElementById('resultSec4').innerHTML = resultSec4;
}
