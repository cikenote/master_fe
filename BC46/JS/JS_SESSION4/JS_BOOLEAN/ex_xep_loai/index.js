document.getElementById('btnXepLoai').onclick = function() {
    var diemToan = document.getElementById('diem-toan').value*1;
    var diemLy = document.getElementById('diem-ly').value*1;
    var diemHoa = document.getElementById('diem-hoa').value*1;
    var diemTB = tinhDiemTrungBinh(diemToan,diemLy,diemHoa);
    var xepLoai = xepLoaiFunc(diemTB);
    console.log(xepLoai);
}
function tinhDiemTrungBinh(diem1, diem2, diem3) {
    var diemTB = diem1 + diem2 + diem3;
    diemTB /= 3;
    return diemTB;
}
function xepLoaiFunc(diemTB) {
    var xepLoai = '';
    if(diemTB>=8.5){
        xepLoai = 'Giỏi';
    } else if(diemTB>=6.5 && diemTB<8.5){
        xepLoai = 'Khá';
    } else if(diemTB>=5 && diemTB<6.5){
        xepLoai = 'Trung Bình';
    } else{
        xepLoai = 'Yếu';
    }
    return xepLoai;
}
