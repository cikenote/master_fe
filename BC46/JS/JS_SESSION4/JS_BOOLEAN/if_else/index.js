document.getElementById('btnTinhDTB').onclick = function tinhDTB() {
        // input: diemToan, diemLy, diemHoa
        var diemToan = document.getElementById('diemToan').value*1;
        var diemLy = document.getElementById('diemLy').value*1;
        var diemHoa = document.getElementById('diemHoa').value*1;
        // output: string
        var ketQua = '';
        // progress
        var xepLoai = '';
        var diemTrungBinh = (diemToan + diemLy + diemHoa) / 3;
        // B2
        if(diemTrungBinh > 0 && diemTrungBinh < 5) {
            xepLoai = 'Không đạt';
        } else if(diemTrungBinh >=5 && diemTrungBinh <8){
            xepLoai = 'Đạt';
        } else {
            xepLoai = 'Giỏi';
        }
        ketQua = `Điểm TB:${diemTrungBinh} - Xếp loại:${xepLoai}`;
        document.getElementById('ketQuaBai4').innerText = ketQua;
}

