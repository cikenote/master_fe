var isLogin = true;
isLogin = false;

// Phép so sánh => true false
var ss1 = 2 < 1;
var ss2 = 2 < 0;
// so sánh bằng(==)
var ss3 = "abccc" == "abccc1";
// so sánh khác (!=)
var ss4 = "alice@gmail.com" != "bob@gmail.com";
var ss5 = 5 != 5;
console.log(ss4,ss5);

// lớn hơn hoặc bằng
var ss7 = 3 >= 3;
console.log(ss7);
var ss8 = 3 <=2;
console.log(ss8);

// toán tử so sánh (kết hợp nhiều phép so sánh với nhau) => true false
// so sánh bằng: & => true, khi TẤT CẢ true
var ss9 = true && false;
console.log(`ss9 - ${ss9}`);
var ss10 = true && false && true && true && true;
console.log(`ss10 - ${ss10}`);
// so sánh hoặc: || => chỉ false khi tất cả đều false
var ss11 = true || false;
