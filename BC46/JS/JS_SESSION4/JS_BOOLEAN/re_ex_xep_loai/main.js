document.getElementById("btnXepLoai").onclick = function () {
  var diemToan = document.getElementById("diem-toan").value * 1;
  var diemLy = document.getElementById("diem-ly").value * 1;
  var diemHoa = document.getElementById("diem-hoa").value * 1;
  var diemTrungBinh = diemTB(diemToan, diemLy, diemHoa);
  document.getElementById("diemTB").innerText = diemTrungBinh;
  var xepLoaiText = xepLoai(diemTrungBinh);
  document.getElementById("xepLoai").innerText = xepLoaiText;
};

let diemTB = (diem1, diem2, diem3) => (diem1 + diem2 + diem3) / 3;

function xepLoai(diemTB) {
  var xepLoai = "";
  if (diemTB >= 8.5) {
    xepLoai = "Giỏi";
  } else if (diemTB >= 6.5 && diemTB < 8.5) {
    xepLoai = "Khá";
  } else if (diemTB >= 5 && diemTB < 6.5) {
    xepLoai = "Trung Bình";
  } else {
    xepLoai = "Yếu";
  }
  return xepLoai;
}
