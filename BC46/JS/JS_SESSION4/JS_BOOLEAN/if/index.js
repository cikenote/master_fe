// if ~ điều kiện rẽ nhánh
var count = 0;
if(2 > 10) {
    console.log("yes hành động");
}
var isLogin=true;
isLogin = false;
if (isLogin) {
    console.log("Đăng nhập thành công");
}
if(!isLogin) {
    console.log("Đăng nhập thất bại");
}
// VÍ DỤ 2: tính tiền phạt thẻ tín dụng
// input: tiền nợ thẻ tín dụng, tiền đã trả
// ouput: tiền phạt chưa thanh toán
/**progress:
 * lãi suất: 1.5% / tháng
 * tiền phạt = (tiền nợ - tiền trả) * 1.5%
 */