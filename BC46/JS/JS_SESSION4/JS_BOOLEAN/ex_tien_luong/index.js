document.getElementById('btnCal').onclick = function tinhLuong() {
    var workingHour = document.getElementById('workingHour').value*1;
    var oneHourWage = document.getElementById('oneHourWage').value*1;
    var OT = workingHour<=40;
    var result = 0;
    if(OT){
        result = workingHour*oneHourWage;
    }
    else{
        result = (40 + (workingHour-40)*1.5)*oneHourWage;
    }
    document.getElementById('result').innerText = result.toLocaleString();
}