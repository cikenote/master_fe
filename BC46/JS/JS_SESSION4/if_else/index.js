// Tìm max của 2 số
document.getElementById('btnEx2').onclick = function maxNo() {
    var no1st = document.getElementById('no1st').value*1;
    var no2nd = document.getElementById('no2nd').value*1;
    // output
    var max = 0;
    // progress: flag techniques
    max = no1st;
    if(max<no2nd){
        max = no2nd;
    };
    document.getElementById('resultEx2').innerHTML = max;
}

document.getElementById('btnEx3').onclick = function wageCal() {
    var workingHourPerWeek = document.getElementById('workingHourPerWeek').value*1;
    var oneHourWage = document.getElementById('oneHourWage').value*1;
    // output
    var wage = 0;
    // progress
    if(workingHourPerWeek <= 40){
        wage = workingHourPerWeek * oneHourWage;
    }
    else{
        wage = 40*oneHourWage + (workingHourPerWeek-40)*oneHourWage*1;
    }
    document.getElementById('resultEx3').innerText = wage;
}

document.getElementById('btnTinhDTB').onclick = function tinhDTB() {
    // input: diemToan, diemLy, diemHoa
    var diemToan = document.getElementById('diemToan').value*1;
    var diemLy = document.getElementById('diemLy').value*1;
    var diemHoa = document.getElementById('diemHoa').value*1;
    // output: string
    var ketQua = '';
    // progress
    var xepLoai = '';
    var diemTrungBinh = (diemToan + diemLy + diemHoa) / 3;
    // B2
    if(diemTrungBinh > 0 && diemTrungBinh < 5) {
        xepLoai = 'Không đạt';
    } else if(diemTrungBinh >=5 && diemTrungBinh <8){
        xepLoai = 'Đạt';
    } else {
        xepLoai = 'Giỏi';
    }
    ketQua = `Điểm TB: ${diemTrungBinh} - Xếp loại: ${xepLoai}`;
    document.getElementById('ketQuaBai4').innerText = ketQua;
}

