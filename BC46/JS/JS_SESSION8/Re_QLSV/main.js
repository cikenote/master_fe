function layThongTinTuForm() {
  var maSV = document.querySelector("#txtMaSV").value;
  var tenSV = document.querySelector("#txtTenSV").value;
  var loaiSV = document.querySelector("#loaiSV").value;
  var diemT = document.querySelector("#txtDiemToan").value * 1;
  var diemV = document.querySelector("#txtDiemVan").value * 1;

  return {
    ma: maSV,
    ten: tenSV,
    loai: loaiSV,
    toan: diemT,
    van: diemV,
  };
}
function showThongTinlenform(tTSV) {
  document.getElementById("spanTenSV").innerText = tTSV.tenSV;
  document.getElementById("spanMaSV").innerText = tTSV.maSV;
  document.getElementById("spanLoaiSV").innerText = tTSV.loaiSV;
  document.getElementById("spanDTB").innerText = tTSV.tinhDTB();
  document.getElementById("spanXepLoai").innerText = tTSV.xepLoai();
}

function hienThiThongTin() {
  //lấy dữ liệu từ form
  var result = layThongTinTuForm();

  //Tạo object
  var tTSV = {
    maSV: result.ma,
    tenSV: result.ten,
    loaiSV: result.loai,
    diemT: result.toan,
    diemV: result.van,
    tinhDTB: function () {
      var DTB = (this.diemT + this.diemV) / 2;
      return DTB;
    },
    xepLoai: function () {
      if (this.tinhDTB() >= 5) {
        return "Đạt";
      } else {
        return "Rớt";
      }
    },
  };
  //Show len form
  showThongTinlenform(tTSV);
}
//gắn sự kiện vào
document
  .getElementById("hienthiThongTin")
  .addEventListener("click", hienThiThongTin);
