var dssv = [];
// lấy dữ liệu từ localStorage lúc user load trang
//#region get data from localStorageg
var dataJson = localStorage.getItem("DSSV");
if(dataJson!=null){
    // map
    dssv = JSON.parse(dataJson).map(item => {
        return new SinhVien(item.ma,item.ten,item.email,item.matKhau,item.toan,item.ly,item.hoa);
    });
    renderDSSV(dssv);
}
//#endregion
function createSinhVien() {
var sv = layThongTinTuForm();
// validate
var isValid = kiemTraTrung(sv.ma, dssv) && 
kiemTraDoDai(5,5,"spanMaSV","mã sv gồm 5 kí tự",sv.ma);
// ktMatKhau
isValid &=
kiemTraDoDai(4,8,"spanMatKhau", "Mật khẩu phải từ 4 đến 8 kí tự", sv.matKhau);
//ktEmail
isValid &= kiemTraEmail(sv.email);
if(isValid) {
    dssv.push(sv);
    // render dssv
    renderDSSV(dssv);
    // save dssv into localStorage
    //#region local Storage : nơi lưu trữ (chỉ chấp nhận file json) , json : 1 loại dữ liệu
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV", dataJson);
    //#endregion
    // reset form
    document.getElementById("formQLSV").reset();
}
}

function deleteSinhVien(id) {
    console.log(id);
    // splice findIndex
    var index = dssv.findIndex(function (item) {
        return item.ma == id;
    });
    dssv.splice(index,1);
    renderDSSV(dssv);
}

function editSinhVien(id) {
    var index = dssv.findIndex(item => {
        return item.ma == id;
    })
    var svUpdate = dssv[index];
    showThongTinLenForm(svUpdate);
}

function updateSinhVien() {
    var sv = layThongTinTuForm();
    dssv.findIndex(item => {
        return item == sv.ma;
    })
}