function hienThiThongTin(){
//lấy dữ liệu từ form
 var result = layThongTinTuForm();

//Tạo object
var tTSV = {
    maSV: result.ma,
    tenSV: result.ten,
    loaiSV: result.loai,
    diemT: result.toan,
    diemV : result.van,
    tinhDTB: function(){
      var DTB=(this.diemT + this.diemV)/2;
      return DTB;
    },
    xepLoai: function(){
     if(this.tinhDTB() >= 5){
      return "Đạt";
     } else {
        return "Rớt";
     }
    },
}
//Show len form
showThongTinlenform(tTSV);
}
//gắn sự kiện vào
document
.getElementById("hienthiThongTin")
.addEventListener("click", hienThiThongTin);