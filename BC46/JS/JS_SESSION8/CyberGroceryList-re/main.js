// ****** Global Variable **********

const form = document.querySelector(".grocery-form");
const alertElement = document.querySelector(".alert");
const grocery = document.getElementById("grocery");
const submitBtn = document.querySelector(".submit-btn");
const container = document.querySelector(".grocery-container");
const list = document.querySelector(".grocery-list");
const clearBtn = document.querySelector(".clear-btn");

const itemList = new ItemList();
let editFlag = false;
let editID = "";

// ****** Call function **********
getLocalStorage();

submitBtn.addEventListener("click", function (e) {
  e.preventDefault();
  if (!editFlag) {
    addItem();
  } else {
    editItem();
  }
});

// ****** Object Items **********
function Items(id, value) {
  this.id = id;
  this.value = value;
}

function ItemList() {
  this.list = [];

  this.addItemMedthod = function (itemObj) {
    this.list.push(itemObj);
  };

  this.findIndexMethod = function (id) {
    var indexItem = -1; //Khởi tạo biến index Item với giá trị -1
    // sử dụng phương thức map để lặp qua các phần tử trong mảng list
    this.list.map(function (item, index) {
      // Kiểm tra xe id của phần tử hiện tại có khớp với id được truyền vào hay không
      if (item.id === id) {
        // Gán giá trị index của phần tử hiện tại cho biến indexItem
        indexItem = index;
      }
    });
    // Trả về giá trị của biến indexItem sau khi lặp qua toàn bộ mảng list
    return indexItem;

    // Cách khác
    //   for (var i = 0; i < this.list.length; i++) {
    //     if (this.list[i].id === id) {
    //       return i; // Trả về chỉ số của phần tử có id khớp
    //     }
    //   }

    //   return -1; // Trả về -1 nếu không tìm thấy phần tử có id khớp
  };

  this.deleteItemMethod = function (id) {
    // Gọi phương thức findIndexMethod để tìm chỉ số của phần tử có id khớp trong mảng list
    var indexItem = this.findIndexMethod(id);
    // Kiểm tra xem chỉ số indexItem có hợp lệ (lớn hơn -1) hay không
    if (indexItem > -1) {
      // Nếu hợp lệ, sử dụng phương thức splice để xóa phần tử khỏi mảng list
      // splice(index, 1) sẽ xóa phần tử tại chỉ số index khỏi mảng
      this.list.splice(indexItem, 1);
    } else {
      // Nếu không tìm thấy phần tử có id khớp, hiển thị thông báo không thể xóa phần tử
      alert("Can't delete item");
    }
  };

  this.getItemMethod = function (id) {
    // Gọi phương thức findIndexMethod để tìm chỉ số của phần tử có id khớp trong mảng list
    var indexItem = this.findIndexMethod(id);
    // Kiểm tra xem chỉ số indexItem có hợp lệ (lớn hơn -1) hay khôngF
    if (indexItem > -1) {
      // Nếu hợp lệ, trả về phần tử tại chỉ số indexItem trong mảng list
      return this.list[indexItem];
    } else {
      // Nếu không tìm thấy phần tử có id khớp, hiển thị thông báo không thể lấy phần tử
      alert("Can't get item");
    }
  };

  this.editItemMehod = function (id, value) {
    // Gọi phương thức findIndexMethod để tìm chỉ số của phần tử có id khớp trong mảng list
    var indexItem = this.findIndexMethod(id);
    // Kiểm tra xem chỉ số indexItem có hợp lệ (lớn hơn -1) hay không
    if (indexItem > -1) {
      // Gán giá trị mới (value) cho thuộc tính value của phần tử tại chỉ số indexItem trong mảng list
      this.list[indexItem].value = value;
    } else {
      // Nếu không tìm thấy phần tử có id khớp, hiển thị thông báo không thể chỉnh sửa phần tử
      alert("Can't edit item");
    }
  };
}

// ****** Local Storage **********
function setLocalStorage(arr) {
  localStorage.setItem("GroceryList", JSON.stringify(arr));
}

function getLocalStorage() {
  // Kiểm tra xem dữ liệu "GroceryList" có tồn tại trong LocalStorage không?
  if (localStorage.getItem("GroceryList")) {
    // Nếu có, lấy dữ liệu từ LocalStorage và chuyển đổi từ chuỗi JSON thành mảng
    itemList.list = JSON.parse(localStorage.getItem("GroceryList"));
  } else {
    // Nếu không, gán một mảng rỗng cho itemList.list
    itemList.list = [];
  }

  // Hiển thị danh sách mua sắm
  showItems();
}

// ****** Functions **********
function addItem() {}

function showItems() {}

function deleteItem(id) {}

function getItem(id) {}

function editItem() {}

function resetForm() {}

function clearItems() {}

function displayAlert(text, action) {}
