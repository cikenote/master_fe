//primitive value: number, string, boolean, null, nuderdefined;
// reference value: array, object

/**
 * object: {} lưu các thông tin khác nhau mô tả cùng 1 đối tượng => key: value
 * 
 *  before: 1 học viên => 3 biến name, phone, age ~ 10 học viên => 30 biến khác nhau
 * afer: 10 object để lưu 10 học viên
 */

// key: value
var dog1 = {
    name: "lulu",
    age: "2",
};
console.log(dog1);

var dog2 ={
    name: "mimi",
    age: "2",
    color: "blue",
    speak: function(){
        console.log("hi hi t la phi:", this.name);
        
    },
};
dog2.speak();
console.log(dog2);
// class: lớp đối tượng

function Dog(ten, tuoi) {
    this.name = ten;
    this.age = tuoi;
    this.speak = function(){
        console.log("gau gau gau")
    }
}
var dog1 = new Dog("lulu", 3);
var dog2 = new Dog("HEHE", 2)
console.log(dog1, dog2);
dog1.speak();