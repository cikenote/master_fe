/**
 * Các yếu tố xác định bởi toán lặp: logic lặp đi lặp lại 1 cách tự động thì đó là bài toán lặp
 * Bước 1: Xác định yếu tố thay đổi (khởi tạo biến bước nhảy)
 * Bước 2: Thiết lập điều kiện lặp
 * Bước 3: Cài đặt khối xử lý
 * Bước 4: Thay đổi giá trĩ của biến bước nhảy
 */
document.getElementById('btnInTheDiv').onclick = function () {
    // input: number
    var input = Number(document.getElementById('iSo').value);
    // output: string
    var output = '';
    // process
    // b1
    var dem = 1;
    // b2
    while(dem<=input){
        // b3
        var div = '<div class="alert alert-success mt-2">Hello cybersoft</div>';
        output += div;
        // b4
        dem++;
    }

    document.getElementById('ketQua1').innerHTML = output;
}

/**Ví dụ 2: Cho phép người dùng nhập vào 1 số tính giai thừa số đó */

document.getElementById('btnTinhGiaiThua').onclick = function() {
    // input: number
    var nhapSo =Number(document.getElementById('nhapSo').value);

    // output: giaiThua = 1;
    var giaiThua = 1;
    // B1:
    var giaTri = 1;
    // B2:
    while(giaTri <= nhapSo) {
        giaiThua *=giaTri;
        giaTri++;
    }
    document.getElementById('ketQua2').innerHTML = giaiThua;
}

// Ví dụ 3: 
document.getElementById('btnTinhTong').onclick = function() {
    // input
    var number = Number(document.getElementById('nhapSo_3').value);
    // output
    var tong = 0;
    // process
    var soHang = 1;
    while(soHang<=number){
        tong += soHang;
        soHang++;
    }
    document.getElementById('ketQua3').innerHTML = tong;

}

// Ví dụ 4:
document.getElementById('btnTinhTongSoChan').onclick = function() {
    // input
    var number = Number(document.getElementById('nhapSo_4').value);
    // output
    var tong  = 0;
    // process
    var soHang = 2;
    while(soHang<=number){
        /**
         * Cách 1:
         * if (soHang %2 === 0){
         * tong += soHang;
         * }
         * soHang++;
         */
        tong += soHang;
        soHang+=2;
    }
    document.getElementById('ketQua4').innerHTML = tong;
}
// Ví dụ 5
// Cách 1
/** 
document.getElementById('btnKiemTraSoNguyenTo').onclick = function() {
    // input
    var number = Number(document.getElementById('nhapSo_5').value);
    // output
    var isNguyenTo = '';
    // process
    if(number!=1&&(number==2||number==3||number==5||number==7)){
        isNguyenTo = 'là số nguyên tố';
    }
    else if(number%2!=0&&number%3!=0&&number%5!=0&&number%7!=0){
        isNguyenTo = 'là số nguyên tố';
    } else{
        isNguyenTo = 'không phải là số nguyên tố';
    }
    document.getElementById('ketQua5').innerHTML = `${number} ${isNguyenTo}`;
}
*/

// Cách 2
/**document.getElementById('btnKiemTraSoNguyenTo').onclick = function() {
    // input
    var number = Number(document.getElementById('nhapSo_5').value);
    // output: string
    var ketQua = '';
    // process
    var soHang = 1;
    var dem = 0;
    while (soHang<=number){
        if(number%soHang==0){
            dem++;
        }
        soHang++;
    }
    if(dem==2){
        ketQua = 'là số nguyên tố';
    }
    else{
        ketQua = 'không phải là số nguyên tố';
    }
    document.getElementById('ketQua5').innerHTML = `${number} ${ketQua}`;
}*/

// Cách 3
document.getElementById('btnKiemTraSoNguyenTo').onclick = function() {
    // input
    var number = Number(document.getElementById('nhapSo_5').value);
    // output: string
    var ketQua = '';

    // Đặt 1 biến mặc định (biển cờ hiệu)
    var checkSNT = true;
    // Bước 1: Yếu tố thay đổi
    var soHang = 2;
    // Bước 2: Xác định yếu tố lặp
    while(soHang<=Math.sqrt(number)){
        // Nếu chia hết cho thêm 1 số nào khác nửa (ước > 2)
        if(number%soHang === 0){
            checkSNT = false;
            break; //=>lệnh toát ra khỏi vòng lặp
        }
        // bước 4: thay đổi giá trị bước nhảy
        soHang++;
    }

    if(checkSNT) {
        ketQua = 'là số nguyên tố';
    }else{
        ketQua = 'không phải là số nguyên tố';
    }
    document.getElementById('ketQua5').innerHTML = `${number} ${ketQua}`;
}

// Ví dụ 6
document.getElementById('btnInSao').onclick = function() {
    // input: number
    var number = document.getElementById('nhapSo_6').value*1;
    // output: string
    var ketQua = '';
    // process
    // Bước 1: xác định value thay đổi
    var soSao = 1;
    // Bước 2: Xác định điều kiện lặp
    // while(soSao<=number){
    //     ketQua +=  '*';
    //     // B4:thay đổi giá trị bước nhảy
    //     soSao++;
    // }
    for(;soSao <= number; soSao++){
        ketQua += '*';
    }

    document.getElementById('ketQua6').innerHTML = `${ketQua}`;
}
/**Ví dụ 7 */
document.getElementById('btnInKetQua').onclick = function() {
    // input: soHang, soCot
    var soHang = Number(document.getElementById('soHang').value);
    var soCot = Number(document.getElementById('soCot').value);
    // output: string
    var ketQua = '';
    // process
    // B1: Xđ value thay đổi 
    // B2: Đk lặp
    // for(var hangNgang = 1;hangNgang<=soHang;hangNgang++){
    //     for(var hangDoc = 1;hangDoc<=soCot;hangDoc++){
    //         ketQua+='*';
    //     }
    //     ketQua+='</br>';
    // }
    // document.getElementById('ketQua7').innerHTML = ketQua;
    // Cách 2:
    for(var i = 1; i<= soHang; i++){
        // Code để sinh ra 1 hàng
        var htmlHangSao = inHangSao(soCot);
        ketQua += htmlHangSao + '</br>';
        document.getElementById('ketQua7').innerHTML = ketQua;
    }
}

function inHangSao(soSao){
    // input: 5
    // output: string *****
    var ketQua = '';
    for(var i = 1; i <= soSao; i++) {
        ketQua += '* ';
    }
    return ketQua;
}

// Ví dụ 8
document.getElementById('btnInSo_8').onclick = function() {
    // input: number từ người dùng
    var number = Number(document.getElementById('nhapSo_8').value);
    // output
    var ketQua = '';

    for(var iSo = 2;iSo<=number;iSo++){
        // Kiểm tra từng số
        // Kiểm tra iSo có phải là số nguyên tố hay không
        var checkSNT = kiemTraSoNT(iSo);
        if(checkSNT){
            ketQua += iSo + ' ';
        }
    }
    document.getElementById('ketQua8').innerHTML = ketQua;
}
function kiemTraSoNT(number){
    // output: true ? false
    var checkSNT = true;

    for(var i = 2; i<=Math.sqrt(number); i++){
        if(number % i === 0){
            checkSNT =false;
            break;
        }
    }
    return checkSNT;
}