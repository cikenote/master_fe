/**
 * VÍ DỤ 1: Viết chương trình yêu cầu người dùng nhập
 * vào 1 giá trị và khi người dùng bấm hiển thị thì giá
 * trị đó tại thẻ span#ketQuaHienThi
 */

function hienThiThongTin() {
  //Lấy giá trị nhập khi gọi hàm hienThiThongTin
  //input: luôn là String
  var input = document.getElementById("giaTriNhap");
  //output: string
  var output = input.value;
  //progress;
  // output = input.value;
  // Xử lý kết quả hiển thị lên giao diện
  var tagSpanKetQua = document.getElementById("ketQuaHienThi");
  tagSpanKetQua.innerHTML = output;
}

/**
 * VÍ DỤ 2:
 */

function tinhTongLuong() {
  var soTienLuong1h = document.getElementById("tienLuong1h").value * 1;
  var soGio = document.getElementById("soGioLam").value * 1;
  var tongLuong = soTienLuong1h * soGio;
  document.getElementById("tongLuong").innerHTML = tongLuong.toLocaleString();
}

/**
 * VÍ DỤ 3: Xây dựng form đăng nhập
 */
var btnDangNhap = document.getElementById("btnDangNhap");
btnDangNhap.onclick = function () { // anonymous function
    // input: taiKhoan: String, matKhau: String
    var taiKhoan = document.getElementById("taiKhoan").value;
    var matKhau = document.getElementById("matKhau").value;
    // output: thông báo: String
    // console.log("taikhoan",taiKhoan);
    // console.log("matkhau",matKhau);
    let thongBao = "";
    thongBao = "Tài khoản: " + taiKhoan + " - Mật khẩu: " + matKhau;
    document.getElementById("ketQuaDangNhap").innerHTML = thongBao;
    // Thay đổi màu sắc
    var tagKetQua = document.getElementById("ketQuaDangNhap");
    // tagKetQua.style.backgroundColor = "green";
    // tagKetQua.style.padding = "15px";
    // tagKetQua.style.color = "#fff"
    tagKetQua.className = "bg-success p-2 m-2 text-white"
}


/**
 * VÍ DỤ 4: Tính tiền típ
 */


document.getElementById("btnTinhTienTip").onclick = function () {
    /**
     * input:
     *      + tongTienThanhToan: number
     *      + PhanTramTip: number
     *      + soNguoiTip: number
     * progress:
     *      + Lấy thông tin người dùng nhập từ giao diện
     *      + Sử dụng công thức tình phần trăm tip quy ra tiền
     *      và chia cho số người
     * out put:
     *      + tienTiepTrenNguoi: number
     */

    var tongTienThanhToan = document.getElementById("tongTienThanhToan").value;
    var phanTramTip = document.getElementById("phanTramTip").value;
    var soNguoiTip = document.getElementById("soNguoiTip").value;
    var tienTiepTrenNguoi = (tongTienThanhToan * phanTramTip / 100) / soNguoiTip;

    document.getElementById("tienTipTrenNguoi").innerHTML = tienTiepTrenNguoi + " $"
}