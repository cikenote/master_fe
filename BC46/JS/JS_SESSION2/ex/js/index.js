var tagh3 = document.getElementById('title');

//.innerHTML
tagh3.innerHTML = "Elearning.edu.vn";

//.value: input of user
var tagInput = document.getElementById('txt');

tagInput.value = 'Hello mr.Khang';

// Extract value of tag
var tagInputNumber = document.getElementById('number');
console.log(tagInputNumber.value);
// alert(tagInputNumber.value);

// .src => content of img
var tagImg = document.getElementById('img');

tagImg.src = './img/hinh2.jpg';
/* function */
function sayHello () {
    alert('Hello customer');
}
  /**Ví dụ 1: Viết chương trình yêu cầu người dùng nhập vào 1 giá trị và khi người dùng bấm hiển thị thì giá trị đó sẽ in ra tại thẻ san#ketQuaHienThi */
  function hienThiThongTin () {
    var input = document.getElementById('giaTriNhap');
    // console.log(input.value);
    // output: string
    var output = '';
    // process
    output = input.value;
    var tagSpanKetQua = document.getElementById('ketQuaHienThi');
    tagSpanKetQua.innerHTML = output;
  }
  function tinhTongLuong() {
    // alert (123);
    // input: tienLuong1h: number, soGioLam: number
    var tienLuong = document.getElementById('tienLuong1h').value;
    var soGioLam = document.getElementById('soGioLam').value;
    console.log(tienLuong, soGioLam);
    // clg = console.log
    // output: tongLuong:number
    var tongLuong = 0;
    // process
    tongLuong = tienLuong * soGioLam;
    document.getElementById('tongLuong').innerHTML = tongLuong.toLocaleString();
    // toLocalestring() => fomart number to string
  }
  
  /**Ví dụ 3: Xây dựng form thông báo đăng nhập */
  // function xuLyDangNhap () {

  // }
  var btnDangNhap = document.getElementById('btnDangNhap');
  // anonymous function
  btnDangNhap.onclick = function xuLyDangNhap() {
    // input: taiKhoan: string, matKhau: string
    var taiKhoan = document.getElementById('taiKhoan').value;
    var matKhau = document.getElementById('matKhau').value;
    // output: thongBao: string
    let thongBao = '';
    // process
    thongBao = 'Tài khoản: ' + taiKhoan + ' - Mật khẩu: ' + matKhau; 
    // alt + z => break line
    document.getElementById('ketQuaDangNhap').innerHTML = thongBao;
    // thay đổi màu sắc
    var tagKetQua = document.getElementById('ketQuaDangNhap');
    tagKetQua.innerHTML = thongBao;
    // tagKetQua.style.backgroundColor = 'green';
    // tagKetQua.style.padding = '15px';
    // tagKetQua.style.color = 'white';
    tagKetQua.className = 'bg-success p-2 m-2 text-white';
  };
  // Ví dụ 4: Tính tiền tip
  document.getElementById('btnTinhTienTip').onclick = function () {
    /**input:
     * tongTienThanhToan: number
     * phanTramTip: number
     * soNguoiTip: number
     * process:
     *  Lấy thông tin người dùng nhập từ giao dịch
     *  Sử dụng công thức tính phầm trăm tip quy ra tiền và chia cho số người
     * output: tienTipTrenNguoi : number
     */
    var tongTienThanhToan = document.getElementById('tongTienThanhToan').value;
    var phanTramTip = document.getElementById('phanTramTip').value;
    var soNguoiTip = document.getElementById('soNguoiTip').value;
    var tienTipTrenNguoi = 0;

    tienTipTrenNguoi = (tongTienThanhToan * phanTramTip / 100) / soNguoiTip;
    document.getElementById('tienTipTrenNguoi').innerHTML = '$' + tienTipTrenNguoi;

  }