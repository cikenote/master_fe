function renderProductList(productArr) {
  var contentHTML = "";
  productArr.reverse().forEach(function (item) {
    var content = `<tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>${item.img}</td>
    <td>${item.desc}</td>
    <td>
    <button 
    onclick=xoaSP(${item.id})
    class="btn btn-danger">Xoa</button>
    <button class="btn btn-warning" onclick=suaSP(${item.id})>Sua</button>
    </td>
                </tr>`;
    contentHTML += content;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  //
}

function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}

function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}


function layThongTinTuForm() {
  var tenSP = document.getElementById("TenSP").value;
  var giaSP = document.getElementById("GiaSP").value;
  var hinhSP = document.getElementById("HinhSP").value;
  var moTaSP = document.getElementById("moTaSP").value;

  // ko có id trong create 
  return {
    name: tenSP,
    price: giaSP,
    img: hinhSP,
    desc: moTaSP,
  };
}

function showThongTinLenForm(product) {
  document.getElementById("TenSP").value = product.name;
  document.getElementById("GiaSP").value = product.price;
  document.getElementById("hinhSP").value = product.img;
  document.getElementById("moTaSP").value = product.desc;
}