var idProductUpDate;
// lấy dssp từ server và render ra layout


function fetchProductList() {
  batLoading();
  productServ
    .getList()
    .then(function (res) {
      renderProductList(res.data);
      // console.log(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
  //
}
fetchProductList();
//   xoá sp
function xoaSP(id) {
  batLoading();
  productServ
    .delete(id)
    .then(function (res) {
      // sau khi xoá thành công trên server thì gọi api lấy danh sách mới nhất từ server
      fetchProductList();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}

function themSP() {
  var newProduct = layThongTinTuForm();
  productServ
    .create(newProduct)
    .then(function (res) {
      // debugger
      // nếu thêm thành công thì gọi lại api lấy danh sách mới nhất từ server
      fetchProductList();
      $("#myModal").modal("hide");
    })
    .catch(function (err) { });
}

function suaSP(id) {
  idProductUpDate = id;
  $("#myModal").modal("show");
  batLoading();
  // gọi api lấy chi tiết
  productServ
    .getById(id)
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data)
    })
    .catch(function (err) {
      tatLoading();
    })
  // đưa thông tin lên form
}

function capNhatSP() {
  var product = layThongTinTuForm();
  productServ
    .update(idProductUpDate, product)
    .then(function (res) {

    })
    .catch(function (err) { });
}

// promise chaining, promise all
