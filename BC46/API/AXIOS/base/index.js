// đồng bộ

// bất đồng bộ
console.log(4);
setTimeout(function () {
  console.log(0);
}, 0);

console.log(2);
console.log(1);

// đồng bộ chạy hết thì mới chạy tiếp bất đồng bộ
// event loop

// api ~ bất đồng bộ

var promise = axios();
// console.log("🚀 - file: index.js:18 - promise", promise);

// pendding , resolve ( success ), reject ( error )

var dssp = [];

axios({
  url: "https://633ec05b0dbc3309f3bc5455.mockapi.io/product",
  method: "GET",
})
  .then(function (res) {
    dssp = res.data;
    console.log(" res", res.data);
  })
  .catch(function (err) {
    console.log("err", err);
  });

console.log(dssp.length);

for (var i = 0; i < 5; i++) {
  setTimeout(function () {
      console.log(i);
  }, 1000);
}
