import "./App.css";
import Header from "./components/Header";
import Product from "./components/Product";
import { Greeting } from "./components/BaiTapTuLuyen/Greeting";

function App() {
  return (
    <div className="App">
      {/* <Header /> */}
      {/* // Tính chất tái sử dụng: có thể coppy nhiều ra nhiều Header khác nhau để tái sử dụng */}
      {/* <Header></Header> */}
      {/* <div className="container">
        <Product />
      </div> */}
      {/* <h1>App Components</h1>
      <Greeting name="alice"/>
      <Greeting name="Bob"/> */}
    </div>
  );
}

export default App;

// Mỗi components là một function
