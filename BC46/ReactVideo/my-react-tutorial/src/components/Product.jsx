import React, { Component } from "react";

// react class component là 1 lớp đối tượng có:
// ==*== Thuộc tính
// ==*== Phương thức

export default class Product extends Component {
  //   ==*== Thuộc tính

  //   ==*== Phương thức

  // Nội dung thẻ được định nghĩa trong hàm render

  render() {
    return (
      <div className="card text-white bg-primary">
        <img className="card-img-top" src="holder.js/100px180/" alt />
        <div className="card-body">
          <h4 className="card-title">Title</h4>
          <p className="card-text">Text</p>
        </div>
      </div>
    );
  }
}
