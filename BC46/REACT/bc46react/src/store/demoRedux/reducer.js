const initialState = {
  number: 1,
  name: "ABCdf",
};

const demoReduxReducer = (state = initialState, action) => {
  console.log("🚀 ~ file: reducer.js:7 ~ demoReduxReducer ~ action:", action);
  return state;
};

export default demoReduxReducer;
