import React from "react";

const RenderWithMap = () => {
  const userList = [
    { name: "John", age: 20, address: "123 Main St" },

    { name: "Jane", age: 30, address: "456 Main St" },

    { name: "Joe", age: 40, address: "789 Main St" },
  ];

  const renderUserList = () => {
    return userList.map((user, index) => {
      return (
        <li key={index}>
          Name: {user.name} - Age: {user.age} - Address: {user.address}
        </li>
      );
    });
  };
  return (
    <div className="container mt-5">
      RenderWithMap
      {/* 1. Phải có các thuộc tính key ở thẻ bao bọc ngoài cùng */}
      {/* 2. Keu phải là giá trị duy nhất, không bị trùng lặp */}
      {/* 3. Hạn chế sử dụng index làm giá trị của thuộc tính key,
      chỉ nên sử dụng khi mảng là mảng tĩnh (không thêm sửa xóa gì ở danh sách render) */}
      <ul>
        {/* {userList.map((user, index) => {
          return (
            <li key={index}> // key là bắt buộc có 
              Name: {user.name} - Age: {user.age} - Address: {user.address}
            </li>
          );
        })} */}                  
        {renderUserList()}
      </ul>
    </div>
  );
};

export default RenderWithMap;

// hàm map

// nói tóm lại map dùng để render ra UI thế hoi mún biết gì thêm thì tự đi mà mò!!!