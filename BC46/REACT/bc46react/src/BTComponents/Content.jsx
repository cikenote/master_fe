import React from 'react'

const Content = () => {
  return (
    <div className='py-5 px-3 bg-warning text-white text-center display-4'>Content</div>
  )
}

export default Content