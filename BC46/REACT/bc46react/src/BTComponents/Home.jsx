import React from "react";
import Header from "./Header";
import Navigation from "./Navigation";
import Content from "./Content";
import Footer from "./Footer";

const Home = () => {
  return (
    <div className="container">
      <h1 className="p-2 bg-dark text-white text-center">Home</h1>
      <Header></Header>
      <div className="d-flex">
        <div className="col-4 p-0">
          <Navigation></Navigation>
        </div>
        <div className="col-8 p-0">
          <Content></Content>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default Home;
