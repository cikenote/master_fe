import React from "react";
import movieList from "./data.json";

const BTMovie = () => {
  return (
    <div>
      <h1>BTMovie</h1>
      <div className="row">
        {movieList.map((movie) => {
          return (
            <div key={movie.maPhim} className="col-3 mt-3">
              <div className="card">
                <img
                  style={{ width: "250px", height: "350px" }}
                  src={movie.hinhAnh}
                  alt="hinhnef"
                />
                <div
                  className="card-body"
                  style={{ height: "200px", overflow: "hidden" }}
                >
                  <p className="card-title font-weight-bold">{movie.tenPhim}</p>
                  <p className="card-text">{movie.moTa}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default BTMovie;
