import React from "react";

const DataBinding = () => {
  const name = "Danh nè";
  const sum = (a, b) => a + b;
  return (
    <div className="container">
      DataBinding
      <p>Hello {name}</p>
      <p>Tổng: {sum(10,20)}</p>
    </div>
  );
};

export default DataBinding;
