// import logo from './logo.svg';
import "./App.css";
import { DemoRedux } from "./DemoRedux/DemoRedux";
import DemoState from "./state/DemoState";
// import BTMovie from "./BTMovie/BTMovie";
// import Home from "./BTComponents/Home";
// import DataBinding from "./DataBinding/DataBinding";
// import DataBindingWithCondition from "./DataBindingWithCondition/DataBindingWithCondition";
// import HandleEvent from "./HandleEvent/HandleEvent";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DemoClassComponent from "./components/DemoClassComponent";
// import DemoFunctionComponent from "./components/DemoFunctionComponent";
// import BTMovie from "./BTMovie/BTMovie";
// import StyleComponent from "./styleComponent/StyleComponent";

// JSX: Javacsript XML => Cho phép các bạn viết HTML 1 cách dễ dàng trong JS
// Binding: tìm hiểu
// attributes: viết theo quy tắc camelCase
// React sử dụng className thay cho class
// Component: thành phần nhỏ của giao diện ứng dụng
// Có 2 loại Component:
// + function component (stateLess component)
// + class component (stateFull component) => *** lifeCircle ***
function App() {
  // const name = "Danh Nè";
  // const sum = (a, b) => a + b;
  return (
    // BT Component
    // <div>
    //   <Home></Home>
    // </div>

    // <DataBinding></DataBinding>

    <div>
      {/* <DataBindingWithCondition></DataBindingWithCondition>

      <HandleEvent></HandleEvent>
      <RenderWithMap></RenderWithMap> */}

      {/* <BTMovie></BTMovie> */}

      {/* <StyleComponent></StyleComponent> */}

      {/* <DemoState /> */}
      <DemoRedux/>
    </div>
  );
}

export default App;
