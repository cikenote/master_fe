import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { rootReducer } from "../store/rootReducer";

export const DemoRedux = () => {
  //  useSelector: dùng để lấy giá trị ở trên sote của reduxx

  // const demoRedux = useSelector((state) => state.demoRedux)
  const { number, name } = useSelector((state) => state.demoRedux);
  // console.log("🚀 ~ file: DemoRedux.js:7 ~ DemoRedux ~ demoRedux:", demoRedux)

  const dispatch = useDispatch();
  return (
    <div className="container mt-5">
      <h1>Demo Redux</h1>

      {/* <p className='display-4'>Number: {demoRedux.number}</p> */}
      <p className="display-4">Number: {number}</p>
      <p className="display-4">Name: {name}</p>

      <div>
        <button
          className="btn btn-success"
          onClick={() => {
            dispatch({
              type: "INCREMENT_NUMBER",
              payload: 1,
            });
          }}
        >
          Increa Number
        </button>
      </div>
    </div>
  );
};
