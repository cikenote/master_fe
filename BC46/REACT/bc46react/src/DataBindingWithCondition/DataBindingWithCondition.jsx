import React from "react";

const DataBindingWithCondition = () => {
  const isShowMessage = true;
  const message = "Hello CongDanh";
  const student = {};
  return (
    <div>
      DataBindingWithCondition
      {/* <p>{isShowMessage ? message : "Hello Cybersoft"}</p> */}
      <p>{isShowMessage && message}</p>
      <p>{student && "Student đang tồn tại"}</p>
      <p>{student && <p>Hello</p>}</p>
    </div>
  );
};

export default DataBindingWithCondition;

// falsy: NaN, undefined, null, o, '', false => khi biến có giá trị là 6 thằng này thì luôn trả về boolean
// Chỉ cần biến student ở trên thuộc falsy thì auto chạy câu lệnh sau &&x