import React, { Component } from 'react'

export default class DemoClassComponent extends Component {
  render() {
    return (
      <div>Cũng sương sương hoi</div>
    )
  }
}

// Nó là 1 class component được kế thừa và được react xây dựng sẵn
// Hàm render trả về một giao diện với một thẻ duy nhất
    