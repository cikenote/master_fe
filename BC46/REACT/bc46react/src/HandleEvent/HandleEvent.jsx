import React from "react";

const HandleEvent = () => {
  const showMessage = () => {
    alert("Message Alert");
  };
  const showMessageParam = (mess) => {
    alert(mess);
  };
  return (
    <div className="container">
      <h1>HandleEvent</h1>
      {/* Trường hợp hàm không có tham số */}
      <button className="btn btn-success mt-3" onClick={showMessage}>
        Click
      </button>
      {/* Trường hợp hàm không có tham số */}
      <button
        className="btn btn-info mt-3 ml-3"
        onClick={() => showMessageParam("Hề lố Công Danh")}
      >
        Click
      </button>
      <button
        className="btn btn-danger mt-3 ml-3"
        // anonymous function
        onClick={() => {
          alert("Click me");
        }}
      >
        Click tôi nè!
      </button>
    </div>
  );
};

export default HandleEvent;

// Có dấu gọi hàm [()] ở showMessage thì khi vào trang trình duyệt sẽ tự động chạy hàm khi không có tham số
// Nếu không có tham số thì bỏ dấu gọi hàm đi giống ở trên ví dụ kìa!
