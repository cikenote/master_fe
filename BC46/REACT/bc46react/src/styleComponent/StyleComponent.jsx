import React from "react";
import "./style.css"; // => ảnh hưởng tới toàn bộ dự án

import style from "./style.module.css"; // => ảnh hưởng đến component được sử dụng thôi
import './style.scss'

const StyleComponent = () => {
  return (
    <div>
      StyleComponent
      <h1 className="title mt-3">Title</h1>

      {/* Style module */}
      <h2 className={style.subTitle}>Sub title</h2>
      <h2 className={style['style']}>Sub title</h2>
      <h2 className={`${style['sub-title']} ${style['mt20']}`}>Description</h2>
      {/* Style inline */}
      <p style={{frontWeight: 700, frontSize: '40px'}}>Dsscription ABC</p>

      {/* SCSS */}
      <button className="button">Button</button>
      
    </div>
  );
};

export default StyleComponent;
