import React from "react";
// import styles from "./index.css"

const Header = () => {
  return (
    <div className="bg-gray-950 p-8">
      <div className="text-white flex container mx-auto lg:w-3/4 justify-between">
        <div className="">
          <p>Start Boostrap</p>
        </div>
        <div className="">
          <ul>
            <li className="mr-6">Home</li>
            <li className="mr-6">About</li>
            <li className="mr-6">Service</li>
            <li className="mr-6">Contact</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Header;
