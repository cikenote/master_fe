import React from "react";
// import Article from "./components/article/Article";
// import Brand from "./components/brand/Brand";
// import CTA from "./components/cta/CTA";
// import Feature from "./components/feature/Feature";
// import { Navbar } from "./components";
import {
  Footer,
  Blog,
  Possibility,
  Features,
  Header,
  WhatGPT3,
} from "./container";
import { CTA, Brand, Navbar } from "./components";
import "./app.css";

const App = () => {
  return (
    <div className="App">
      <div className="gradient__bg">
        <Navbar></Navbar>
        <Header></Header>
      </div>
      <Brand />
      <WhatGPT3 />
      <Features />
      <Possibility />
      <CTA />
      <Blog />
      <Footer />
    </div>
  );
};

export default App;
